# Version Control

## Introduzione

Nasce la necessità di avere uno storico delle versioni del nostro lavoro, di organizzarlo in modo efficace e di collaborare con più persone sullo stesso progetto. Git, in particolare, sta per **Global Information Tracker** ed è un software per tenere traccia delle modifiche del codice.

Git è un software di controllo versione distribuito utilizzabile da interfaccia a riga di comando, creato da Linus Torvalds nel 2005.

I repository online e locali possono essere divisi in ramificazioni (Branch). I branch (ramificazioni) permettono di creare delle versioni assestanti del codice master. Queste versioni "assestanti" permettono la creazione di features o aggiornamenti in fase di sviluppo che non vanno ad intaccare minimamente il codice del progetto. Finita la scrittura della ramificazione, solitamente, il branch verrà unito con il master.

## Repository

Una **repository** è il contenitore del nostro codice (archivio). In genere, alla parola "repo" corrisponde il progetto su cui stiamo lavorando: un progetto, una repository. La repository può essere locale o remota.  
Git lavora con i repository. Un repository git ha 4 stati di lavoro. Il primo è la tua directory corrente. Il secondo è l'index che fa da spazio di transito per i files (git add \*). Il terzo è l'head che punta all'ultimo commit fatto (git commit -m "messaggio"). E l'ultimo è il repository(remoto) online (git push server).

## Configurazioni di base di git

Configuriamo il nostro git con le nostre credenziali di GitHub:  
`git config --global user.name 'Tuo Nome GitHub'`  
`git config --global user.email email@github.com`

Ci sono due modi per instanzire un progetto

1. Inizializziamo un progetto non esistente:

- `git init`

2. Inizializziamo un progetto esistente su un server git:

- `git clone serverURL.git`  
  Esempio: git clone https://github.com/tesseslol/irixos-websites.git  
  **Git clone permette di copiare il .git file del server e anche il repository.**

## Configurazione del server remoto

Con questo comando visualizziamo la lista di server remoti salvati con relativo url:  
`git remote -v`  
P.S. di solito il server principale si chiama origin

Ora aggiungiamo un server remoto:  
`git remote add identificatoreServerRemoto UrlServerRemoto`  
Esempio: git remote add origin https://github.com/tesseslol/irixos-websites.git

## Comandi Base per lavorare nel progetto

- Aggiungiamo i file dalla directory del progetto all'index (area di staging):  
  `git add nome_file`  
  Si può utilizzare l'asterisco per aggiungere tutti i file.(`git add *`). Se si vuole escludere un file dalla selezione totale (con l'asterisco) basta creare un file denominato `.gitignore` e metterci all'interno i file che non si vogliono aggiungere all' INDEX.

- Ora aggiungiamo i file dell'index all'head:  
  `git commit -m "Messaggio del commit"`  
  Per non tracciare il file usiamo l'argomento -a:  
  `git commit -a -m "Messaggio del commit"`

- Annullamento dei commit(aggiungere le ultime modifiche al commit precedente):  
  `git commit --amend`

- Aprire l'editor di test per scrivere piu commenti specifici all'interno del file:  
  `git commit`

- Cancellare un file da git:  
  `git rm nomeFile`

- Il file ritorna allo stato precedente dell’ultimo commit:  
  `git checkout -- nomeFile`

- Rimuovere un file dalla cartella Git(ma non dalla memoria locale del pc):  
  `git rm --cached nomeFile`

- Rinominare un file (dobbiamo ricordarci poi di aggiungere sempre il file a Git e poi committare):  
  `mv nomefile nuovonomeFile`

- Rinominare in modo piu corretto un file:  
  `git mv nomefile nuovonomefile`  
  PS: Meglio usarlo sempre per rinominare i file, perché Git riesce a capire che stiamo facendo entrambe le operazioni (modifica e rinomina) e non perdiamo mai informazioni relative alla storia del file.

  ## Lavorare con il server remoto

- `git pull`: Aggiorna la tua repo locale al commit più recente.
- `git push nomeserver nomedelbranch`: Invia le modifiche in locale al server remoto.
- `git push origin main`: Invio le modifiche in locale al branch principale in remoto.
- `git push -u origin nomedelbranch`: Imposta il branch remoto di default per i push successivi.
- `git remote rename identificatoreServerRemoto nomeFileVecchio nomeFileNuovo`: Se vogliamo rinominare un file in remoto.
- `git remote rm nomeFile`: Se vogliamo eliminare un file in remoto.

### Git Fetch

Per aggiungere in locale una repository remota possiamo utilizzare:

- `git branch -r` (per controllare le branch remote presenti)
- `git fetch origin` (nel caso in cui il link remoto sia chiamato origin)
- git checkout "nome-locale" "nome-remoto"
- git checkout "nome-locale"

Se il tuo workspace ha file non committati e vuoi copiare le ultime modifiche da un repository remoto direttamente nella tua directory di lavoro, usa il comando git pull.

Se vuoi scaricare le ultime modifiche da un repository remoto senza sovrascrivere nulla nella tua directory di lavoro,  
 allora usa git fetch, e poi fai un git merge quando è il momento giusto

`git remote -v` lista le repo remote aggiunte (origin, ecc.)

## Visualizziamo la storia di git e lo stato del progetto

Stato del progetto Comandi:

- Per vedere le modifiche del progetto digitiamo:  
  `git status`
- Per vedere i cambiamenti dei singoli files digitiamo:  
  `git diff`
- Vedere tutti i commit:  
  `git log`

I comandi principali per visualizzare la storia di Git sono `log`, `show` e `diff`. Questi comandi hanno molte opzioni per formattare l'output.

- `git log`: Visualizza cosa abbiamo fatto fino ad adesso(tutti i commit)  
  Scorrendo con le frecce nel terminale, visualizzeremo ogni commit che abbiamo fatto fino ad ora. Accanto alla voce "commit" troviamo degli ID.

### **Breve ripasso di hash**:

Ad ogni commit corrisponde un output che non è altro che l'ID del nostro commit. L'output ha lunghezza fissa,è unico e non è reversibile. Se modifichiamo anche minimamente un file, l'hash avrà un output completamente diverso dal precedente.

- `git show hashid`: Mostra i dettagli di uno specifico commit copiando l'ID o le prime lettere dell'ID.
- `git diff`: Confronta due punti della storia.
  - `git diff --stat`: Mostra quante righe sono state modificate, aggiunte o eliminate.
  - `git diff nomefile`: Visualizza solo quello che è accaduto in un file specifico.
  - `git diff hashprimoId hashSecondoId`: Confronta due commit specifici.

### Navigare nella Storia del Codice

- `git checkout`: Permette di navigare la storia del codice.
  - `git checkout IDcommit`: Sposta il puntatore HEAD a un momento preciso della storia.
  - Git ha un HEAD, che è come un puntatore che indica un momento preciso della storia.

## Gestione dei Branch

Ad un certo punto può essere necessario creare più branch per vari lavori.

- `git branch`: Lista dei vari rami
- `git branch nomeBranch`: Creo un nuovo branch
- `git checkout -b nome_branch`: Crea un nuovo branch dal punto in cui ci troviamo e si sposta in quel branch
- `git checkout nomeBranch`: Cambio branch
- `git checkout master/main`: Torinamo al branch principale (di default master o main)
- `git branch -d nome_branch`: Elimina un branch.

Per unire il branch al repository originale usiamo (ricordatevi di fare un commit nel branch):

- `git checkout master`: Ci spostiamo nel branch master ed effettuiamo un merge con il branch che vogliamo unire
- `git merge nomeBranch`
  **In caso di conflitti, Git ci permette di scegliere tra le varie modifiche.**

## Rebase

Il comando `git rebase` in Git è utilizzato per integrare le modifiche di un branch in un altro, riscrivendo la cronologia dei commit. A differenza di `git merge`, che crea un nuovo commit di merge che unisce due storie di sviluppo, git rebase prende i commit da un branch e li "applica" in cima a un altro, risultando in una storia lineare.

Immaginiamo di avere due branch, main e feature. Hai lavorato su feature e hai alcuni commit che vuoi integrare in main.

1. Situazione attuale :

```zsh
main
──A──B──C
          \
feature     D──E──F
```

2. Passiamo al branch `feature`

3. Rebasiamo il branch `feature` sopa `main`  
   `git rebase main`  
   Questo comando prende i commit di feature (D, E, F) e li riapplica in cima all'ultimo commit di main (C).

4. La situazione nuova sarà:

```zsh
main
──A──B──C──D'──E'──F'
```

5. Risoluzione dei conflitti:  
   Durante il rebase, potrebbero esserci conflitti che devono essere risolti manualmente. Git ti guiderà attraverso il processo di risoluzione dei conflitti, e una volta risolti, potrai continuare il rebase con:  
   `git rebase --continue`

6. Aggiorna il branch remoto:  
   Dopo aver completato il rebase, dovrai forzare il push del branch feature al remoto, perché la cronologia è stata riscritta:
   `git push origin feature --force`

### Rebase interattivo

Per un controllo più preciso, possiamo usare il rebase interattivo. (mi sto rifacendo sempre all'esempio sopra)

1. Avviamo il rebase interattivo:  
   `git rebase -i main`
2. Apparirà l' editor di default che ci permetterà di scegliere quali commit modificare, combinare, ecc.

Utilita di `git rebase`:

- Tienere il lavoro allineato con quello degli altri.
- Ridurre i conflitti.
- Modificare commit, unirli, ecc.

## Fork

Un fork permette di avere una copia completa di un progetto e farla vivere in completa autonomia.

## Comandi Utili

- `git push origin nomebranch`: Aggiorna una pull request.
- `git push -f nomebranch`: Forza il push dopo un rebase, permette di sovrascrivere il branch remoto (Usare questa cosa solo su branch "personali")
- `git commit --amend`: Modifica l'ultimo commit. Dovremo forzare il push per riallineare il commit remoto e locale.

### Reset e Revert

- `git reset origin main`: Riallinea il branch locale con quello remoto.
- `git reset --hard head~10`: Elimina gli ultimi 10 commit.
- `git revert commitID`: Ripristina lo stato di un commit, creando un nuovo commit.
- `git cherry-pick commitId`: Prende un commit specifico e lo applica al branch attuale creando un nuovo commit

### Altri Comandi Utili

- `git blame`: Mostra chi ha modificato cosa e quando.
- `git checkout .`: Elimina tutte le modifiche non aggiunte all'area di staging.
- `git add -p`: Aggiunge solo una parte delle modifiche all'area di staging.
- `git config user.name "username"`
- `git tag stringa`: Aggiunge un tag (solitamente la stringa corrisponde alla versione della nostra release).

## Versionamento Semantico

**4.2.0** --> MAJOR.MINOR.PATCH

- `4` --> **Versione MAJOR**: Quando modifichiamo l'API in modo non retrocompatibile.
- `2` --> **Versione MINOR**: Quando aggiungiamo funzionalità in modo retrocompatibile.
- `0` --> **Versione PATCH**: Quando correggiamo bug in modo retrocompatibile.

## Pre-Commit

sfrutta gli hook(ganci) di git, per mettersi in mezzo tra il commit e ciò che scriviamo.  
Esempio:

Prima installo `pre-commit`.

- Troviamo il comando per rinstallare pre commit a seconda sel nostro sistema operativo `pip install pre-commit`.

- `pre commit` richiede un file di configurazione (pre-commit-config) che va modificato a seconda di quello di cui necessitiamo e del linguaggio che stiamo utilizzando

- Poi lanciamo una sola volta per repository(all'inizio del progetto), il comando `pre-commit install`
  ( **`pre commit` si va a posizionare in mezzo al commit, quando lanciamo il commit**)

- Una volta lanciato il commit, se lui si accorge che c'è qualcosa che non va, lo segnala e impedisce di committare, funge anche da formatter se vogliamo.
  Quindi noi facciamo la modifica e riproviamo a committare, fino a che non riusciremo a committare.

  ## Le CI

  Sono azioni che vengono eseguite sulla piattaforma remota (gitlab/github)  
  Voglio assicurarmi che l'intero progetto risponda a dei canoni precisi. (possiamo farlo sia con github, sia con git lab)

  Creiamo la nostra prima gitlab CI

  1. Andiamo ad aprire l'editor di gitlab e vediamo la configurazine del file `.gitlab-ci.yml`  
     ![alt text](../img/image.png)

  2. Le varie modifiche ad esempio per far si che la pulizia del file avvenga, le facciamo in lint-test-job
     ![alt text](../img/image-1.png)

  3. Al posto dele configurazioni di default, ad esempio installiamo ruff e lo lanciamo. quindi script: lo script che ci serve

  4. Abbiamo anche la possibilità di scegliere noi cosa deployare e quando:
     ![alt text](../img/image-2.png)

Le CI esistono in container, grazie a questo è possibile specificare l'immagine da utilizzare e i software installati su di esso.
Possono inoltre essere impostate per funzionare solo su determinate branch.
